

package ws.very.translator

import ws.very.translator.http
import ws.very.translator.langs.VI
import ws.very.util.lang.AnyInputExitableApp
import ws.very.util.lang.InputExitApp

object Test extends AnyInputExitableApp with InputExitApp {
  //  import dispatch._, Defaults._
  //  val svc = url("http://translate.google.com/translate_a/t?client=t&hl=zh-TW&sl=auto&tl=zh-CN&ie=UTF-8&oe=UTF-8&multires=1&oc=2&otf=1&ssel=4&tsel=4&pc=1&sc=1&q=i%20am%20good")
  //
  //  val country = Http(svc OK as.String).either
  //  country.foreach{println}
  import ws.very.translator._
  import langs.VI
  val bindAppId = "TM8zwDC7ucDddlK4zTjzHhZ_CwlBFx97BtWmR57UtdjXwUfOSAxjuqT3F_7SAbMyT"
  val Bind = BingTrans(bindAppId)
  val Google = GoogleTrans()
  val txt = """Success as the only option 
Earlier we compared futures to options. The network operation at the center of things may or may not have completed: that’s the temporal uncertainty and it can be thought of an option, and even transformed into one with the completeOption method.
Beyond that we don’t know if a completed future will produce an error or a useful response. We can also think of that uncertainty, and model it in code, as an option. By transferring the uncertainty from the future to a contained option, we make a future that will never fail."""
  //  while (true) {

  //  val rq = Google.translate(txt)
  //  //  rq.resStr.foreach { println }
  //  rq.rs.foreach { _.left.foreach(println) }
  //
  //  val brq = Bind.translate(txt)
  //  //  brq.resStr.foreach { println }
  //  //  brq.parseJson.foreach { println }
  //
  //  val brq2 = Bind.translate("i am")
  //  //  brq2.resStr.foreach { println }
  //  //  brq2.parseJson.foreach { println }
  //
  //  brq.rs.foreach { _.left.foreach(println) }
  //  brq2.rs.foreach { _.left.foreach(println) }
  //  }
  def trans(s: String) =
    (Bind.translate(s).rs zip Google.translate(s).rs)
  //  trans("Bigasoft Total Video Converter v3.5.23.4371 + Serial [rahultorrents]").foreach { println }
  // Future.sequence
  List(
    """Shiroka.reka.(17.seriya.iz.24).2008.avi	Shiroka.reka.(17.seriya.iz.24).2008.avi""",
    txt //      ,
    //    """Bigasoft Total Video Converter v3.5.23.4371 + Serial [rahultorrents]\Serial.txt""",
    //    """Bigasoft Total Video Converter v3.5.23.4371 + Serial [rahultorrents]\Installation Instructions.txt""",
    //    """Bigasoft Total Video Converter v3.5.23.4371 + Serial [rahultorrents]\Torrent downloaded from AhaShare.com.txt""",
    //    """Bigasoft Total Video Converter v3.5.23.4371 + Serial [rahultorrents]\1337x.org.txt""",
    //    """Bigasoft Total Video Converter v3.5.23.4371 + Serial [rahultorrents]\Torrent downloaded from Silvertorrents.me.txt"""
    ).foreach { s =>
      //trans(s).foreach(println)
      val t = Bind.translate(s)

//      t.parseJson.foreach(g => Console println s"$s bind parseJson= $g")
//      t.json2Obj.foreach(g => Console println s"$s bind json2Obj= $g")
//      t.fixRes.foreach(g => Console println s"$s bind fixRes= $g")
//      t.resStr.foreach(g => Console println s"$s bind resStr= $g")
      t.rs.foreach(g => Console println s"$s bind rs= $g")

      val google = Google.translate(s)
//      google.parseJson.foreach(g => Console println s"$s google parseJson= $g")
//      google.json2Obj.foreach(g => Console println s"$s google json2Obj= $g")
//      google.fixRes.foreach(g => Console println s"$s google fixRes= $g")
//      google.resStr.foreach(g => Console println s"$s google resStr= $g")
      google.rs.foreach(g => Console println s"$s google rs= $g")

    }

}