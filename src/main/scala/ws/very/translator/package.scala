package ws.very

import org.json4s.DefaultFormats
import com.ning.http.client.AsyncHttpClient
import com.ning.http.client.AsyncHttpClientConfig
import dispatch.Http
import ws.very.util.lang.Implicits2
import scala.concurrent.ExecutionContext
import dispatch.EnrichedFuture
import scala.concurrent.Future
/**
 *
 * log4j:WARN No appenders could be found for logger (com.turn.ttorrent.common.Torrent).
 * log4j:WARN Please initialize the log4j system properly.
 * log4j:WARN See http://logging.apache.org/log4j/1.2/faq.html#noconfig for more info.
 * 2014-3-3 0:50:04 org.jboss.netty.channel.DefaultChannelFuture
 * 警告: An exception was thrown by ChannelFutureListener.
 * java.lang.NullPointerException
 * at com.ning.http.client.listenable.ExecutionList.run(ExecutionList.java:113)
 * at com.ning.http.client.listenable.AbstractListenableFuture.done(AbstractListenableFuture.java:67)
 * at com.ning.http.client.providers.netty.NettyResponseFuture.abort(NettyResponseFuture.java:340)
 * at com.ning.http.client.providers.netty.NettyConnectListener.operationComplete(NettyConnectListener.java:107)
 * at org.jboss.netty.channel.DefaultChannelFuture.notifyListener(DefaultChannelFuture.java:427)
 * at org.jboss.netty.channel.DefaultChannelFuture.notifyListeners(DefaultChannelFuture.java:418)
 * at org.jboss.netty.channel.DefaultChannelFuture.setFailure(DefaultChannelFuture.java:380)
 * at org.jboss.netty.channel.socket.nio.NioClientBoss.processConnectTimeout(NioClientBoss.java:140)
 * at org.jboss.netty.channel.socket.nio.NioClientBoss.process(NioClientBoss.java:83)
 * at org.jboss.netty.channel.socket.nio.AbstractNioSelector.run(AbstractNioSelector.java:312)
 * at org.jboss.netty.channel.socket.nio.NioClientBoss.run(NioClientBoss.java:42)
 * at java.util.concurrent.ThreadPoolExecutor$Worker.runTask(Unknown Source)
 * at java.util.concurrent.ThreadPoolExecutor$Worker.run(Unknown Source)
 * at java.lang.Thread.run(Unknown Source)
 *
 *
 *
 *
 *
 * json TryParse 及 扩展
 */
package object translator extends Implicits2 {

  implicit val formats = DefaultFormats
  object langs {
    implicit val ZH_CH = "zh-ch"
    implicit val VI = "vi"
  }
  implicit val http = Http(new AsyncHttpClient(new AsyncHttpClientConfig.Builder()
    .setConnectionTimeoutInMs(5000).setRequestTimeoutInMs(5000).setMaxRequestRetry(3)
    .build()))
  implicit val exec = ExecutionContext.Implicits.global

  implicit def enrichFuture[T](future: Future[T]) =
    new EnrichedFuture(future)
}