organization := "ws.very.translator"

name := "very-translator"

version := "0.0.1"

scalaVersion := "2.10.3"

resolvers +=
  "oschina maven" at "http://maven.oschina.net/content/groups/public/"

resolvers +=
  "oschina maven thirdparty" at "http://maven.oschina.net/content/repositories/thirdparty/"

//libraryDependencies += "ws.very.util" % "very-util-lang" % "0.0.7"

libraryDependencies ++= Seq(
"net.databinder.dispatch" %% "dispatch-core" % "0.11.0"
, "org.json4s" %% "json4s-native" % "3.2.7"
).map{_.withSources().withJavadoc()}  